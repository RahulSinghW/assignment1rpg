﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1RPG
{
    public class Daggers : Weapon
    {
        public Daggers()
        {
            Name = EnumItemNames.Dagger;
            BaseDamage = 4;
            AttackSpeed = 10;
            ReqiuredLevel = 6;
            DPS = BaseDamage * AttackSpeed;
        }
        
    }
}
