﻿using Assignment1RPG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Assignment1RPGTests
{
    public class ItemAndEquipmentTests
    {
        [Fact]
        public void Equip_EquipWeaponHighLevel_ShouldReturnException()
        {
            //Arange
            Warrior warrior = new Warrior();
            Axe axe = new Axe();

            //Act & Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(axe));

        }

        [Fact]
        public void Equip_EquipArmorHighLevel_ShouldReturnException()
        {
            //Arange
            Warrior warrior = new Warrior();
            Plate plate = new Plate();

            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(plate));
        }

        [Fact]
        public void Equip_EquipWrongWeapon_ShouldReturnException()
        {
            //Arange
            Warrior warrior = new Warrior();
            Bows bows = new Bows();

            //Act & Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(bows));
        }

        [Fact]
        public void Equip_EquipWrongArmor_ShouldReturnException()
        {
            //Arange
            Warrior warrior = new Warrior();
            Cloth cloth = new Cloth();

            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(cloth));
        }

        [Fact]
        public void Equip_EquipValidWeapon_ShouldEquipWeapon()
        {
            //Arange
            Warrior warrior = new Warrior();
            Axe axe = new Axe();
            warrior.LevelUp(4);

            string expected = "New weapon equipped!";
            //Act
            string actual = warrior.EquipWeapon(axe);

            //Assert
            Assert.Equal(expected,actual);
        }

        [Fact]
        public void Equip_EquipValidArmor_ShouldEquipArmor()
        {
            //Arange
            Warrior warrior = new Warrior();
            Plate plate = new Plate();
            warrior.LevelUp(4);

            string expected = "New armour equipped!";
            //Act
            string actual = warrior.EquipArmor(plate);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Calculate_CalulateNoWeaponDPS_ShouldReturnDPS()
        {
            //Arange
            Warrior warrior = new Warrior();

            int expected = 1 * (1 + (5 / 100));
            //Act
            int actual = warrior.TotalHeroDPS();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Calculate_CalulateValidWeaponDPS_ShouldReturnDPS()
        {
            //Arange
            Warrior warrior = new Warrior();
            Hammers hammers = new Hammers();
            warrior.EquipWeapon(hammers);

            int expected = 11 * 2 * (1 + (5 / 100));
            //Act
            int actual = warrior.TotalHeroDPS();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Calculate_CalulateValidWeaponAndArmorDPS_ShouldReturnDPS()
        {
            //Arange
            Warrior warrior = new Warrior();
            Hammers hammers = new Hammers();
            Mail mail = new Mail();
            warrior.EquipWeapon(hammers);
            warrior.EquipArmor(mail);

            int expected = 11 * 2 * (1 + ((5 + 1)/ 100));
            //Act
            int actual = warrior.TotalHeroDPS();

            //Assert
            Assert.Equal(expected, actual);
        }



    }
}
