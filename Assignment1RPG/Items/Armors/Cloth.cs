﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1RPG
{
    public class Cloth : Armor
    {
        
        public Cloth()
        {
            Name = EnumItemNames.Cloth;
            ReqiuredLevel = 2;

            ArmorPrimaryAttributes = new PrimaryAttribute()
            {
                Strength = 3,
                Dexterity = 2,
                Vitality = 4,
                Intelligence = 2
            };
        }
    }

}
