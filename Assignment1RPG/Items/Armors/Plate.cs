﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1RPG
{
    public class Plate : Armor
    {
        public Plate()
        {
            Name = EnumItemNames.Plate;
            ReqiuredLevel = 3;
            PrimaryAttribute armorPrimaryAttributes = new PrimaryAttribute()
            {
                Strength = 2,
                Dexterity = 6,
                Vitality = 3,
                Intelligence = 4
            };
        }
    }
}
