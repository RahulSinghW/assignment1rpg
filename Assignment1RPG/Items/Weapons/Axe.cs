﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1RPG
{
    public class Axe : Weapon
    {
        
        public Axe()
        {
            Name = EnumItemNames.Axe;
            BaseDamage = 2;
            AttackSpeed = 3;
            ReqiuredLevel = 4;
            DPS = BaseDamage * AttackSpeed;

        }
    }
}
