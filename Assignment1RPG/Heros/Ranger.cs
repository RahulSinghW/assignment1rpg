﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1RPG
{
    public class Ranger : Hero
    {
        Armor Armor;
        Weapon Weapon;
        public Ranger()
        {
        }

        public override void CreateHero()
        {
            Name = EHeroNames.Ranger;

            if (NewLevel > Level)
            {
                Level = NewLevel;
            }

            BasePrimaryAttribute = new PrimaryAttribute()
            {
                Strength = 1,
                Dexterity = 7,
                Vitality = 8,
                Intelligence = 1
            };

            HeroAttributeBoost = ((1) / 100) * (BasePrimaryAttribute.Dexterity + ((Level - 1) * 5));

            PrimaryAttribute = new PrimaryAttribute()
            {
                Strength = BasePrimaryAttribute.Strength + (1 * (Level -1)),
                Dexterity = BasePrimaryAttribute.Dexterity + ((Level - 1) * 5) + HeroAttributeBoost,
                Vitality = BasePrimaryAttribute.Vitality + ((Level - 1) * 2),
                Intelligence = BasePrimaryAttribute.Intelligence + (1 * (Level -1)) 
            };

        }



        public override string EquipWeapon(Weapon weapon)
        {

            if (weapon is not (Daggers or Swords))
            {
                throw new InvalidWeaponException("Character cannot equip: " + weapon.Name);
            }
            else if (Level < weapon.ReqiuredLevel)
            {
                throw new InvalidWeaponException("You need to be level " + weapon.ReqiuredLevel + " to equip this weapon: " + weapon.Name);
            }
            else
            {
                EquippedWeapon = "Weapon Equipped";
                Weapon = weapon;
                return new string("New weapon equipped!");
            }

        }

        public override string EquipArmor(Armor armor)
        {
            if (armor is not (Leather or Mail))
            {
                throw new InvalidArmorException("Character cannot equip: " + armor.Name);
            }
            else if (Level < armor.ReqiuredLevel)
            {
                throw new InvalidArmorException("You need to be level " + armor.ReqiuredLevel + " to equip this armor: " + armor.Name);
            }
            else
            {
                EquippedArmor = "Armor Equipped";
                Armor = armor;
                return new string("New armour equipped!");
            }
        }

        public override Armor ArmorEquiped()
        {
            return Armor;

        }

        public override Weapon WeaponEquiped()
        {
            return Weapon;
        }


        public override int TotalHeroDPS()
        {
            if (EquippedWeapon == "Weapon Equipped")
            {
                totalDPS = WeaponEquiped().DPS * (1 + (int)TotalDexterity / 100);
                return totalDPS;
            }
            else
            {
                totalDPS = 1;
                return totalDPS;
            }
        }



    }
}
