﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1RPG
{
    public abstract class Armor : Item
    {
        public PrimaryAttribute ArmorPrimaryAttributes { get; set; }
    }
}
