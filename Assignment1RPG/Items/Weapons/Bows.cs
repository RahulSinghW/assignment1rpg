﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1RPG
{
    public class Bows : Weapon
    {
        public Bows()
        {
            Name = EnumItemNames.Bow;
            BaseDamage = 5;
            AttackSpeed = 6;
            ReqiuredLevel = 8;
            DPS = BaseDamage * AttackSpeed;
        }
    }
}
