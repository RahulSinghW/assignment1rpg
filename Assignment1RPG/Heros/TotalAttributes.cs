﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1RPG
{
    public class TotalAttributes
    {
        public int TotalStrength { get; set; }
        public int TotalDexterity { get; set; }
        public int TotalIntelligence { get; set; }
        public int TotalVitality { get; set; }

    }
}
