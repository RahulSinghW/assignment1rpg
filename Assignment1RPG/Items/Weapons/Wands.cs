﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1RPG
{
    public class Wands : Weapon
    {
        public Wands()
        {
            Name = EnumItemNames.Wand;
            BaseDamage = 13;
            AttackSpeed = 9;
            ReqiuredLevel = 11;
            
        }
    }
}
