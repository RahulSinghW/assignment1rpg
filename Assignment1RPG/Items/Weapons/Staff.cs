﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1RPG
{
    public class Staff : Weapon
    {
        public Staff()
        {
            Name = EnumItemNames.Staff;
            BaseDamage = 2;
            AttackSpeed = 3;
            ReqiuredLevel = 2;
            DPS = BaseDamage * AttackSpeed;
        }

    }
}
