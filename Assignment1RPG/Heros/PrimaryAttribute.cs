﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1RPG
{
    public class PrimaryAttribute
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Vitality { get; set; }

        public static PrimaryAttribute operator +(PrimaryAttribute lhs, PrimaryAttribute Rhs)
        {
            return new PrimaryAttribute
            {
                Dexterity = lhs.Dexterity + Rhs.Dexterity,
                Strength = lhs.Strength + Rhs.Strength,
                Vitality = lhs.Vitality + Rhs.Vitality,
                Intelligence = lhs.Intelligence + Rhs.Intelligence
            };
        }
    }
}
