﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1RPG
{
    public class Mail : Armor
    {
        public Mail()
        {
            Name = EnumItemNames.Mail;
            ReqiuredLevel = 1;
            PrimaryAttribute armorPrimaryAttributes = new PrimaryAttribute()
            {
                Strength = 2,
                Dexterity = 6,
                Vitality = 3,
                Intelligence =  4
            };
        }
        
    }
}
