﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1RPG
{
    public abstract class Hero
    {
        
        public EHeroNames Name;
        public int Level = 1;
        public int TotalStrength { get; set; }
        public int TotalDexterity { get; set; }
        public int TotalVitality { get; set; }
        public int TotalIntelligence { get; set; }

        public PrimaryAttribute BasePrimaryAttribute { get; set; }
        public PrimaryAttribute PrimaryAttribute { get; set; }

        public SecondaryAttribute SecondaryAttribute { get; set; }

        public TotalAttributes TotalAttributes { get; set; }

        public int NewLevel;

        public int totalDPS;

        public int HeroAttributeBoost;

        StringBuilder myString = new StringBuilder("");


        public string EquippedArmor = "Armor not equipped";
        public string EquippedWeapon = "Weapon not equipped";

        /// <summary>
        /// A method to create a hero. In the method primary attributes, name and level is set
        /// </summary>
        public abstract void CreateHero();

        /// <summary>
        /// Method to check if armor equiped is valid or not high level.
        /// Custom exception will be thrown if required level is higher then hero level.
        /// custom exception will be thrown if weapon is inavild.
        /// </summary>
        /// <param name="armor"></param>
        /// <returns>
        /// if armor is valid method return a string message
        /// </returns>
        public abstract string EquipArmor(Armor armor);

        /// <summary>
        /// Method to check if weapon equiped is valid or not high level.
        /// Custom exception will be thrown if required level is higher then hero level.
        /// custom exception will be thrown if weapon is inavild.
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>
        /// if weapon is valid method return a string message
        /// </returns>
        public abstract string EquipWeapon(Weapon weapon);


        /// <summary>
        /// A method to see which armor is currently equipped
        /// </summary>
        /// <returns>
        /// equipped armor
        /// </returns>
        public abstract Armor ArmorEquiped();

        /// <summary>
        /// A method to see which weapon is currently equipped
        /// </summary>
        /// <returns>
        /// equipped weapon
        /// </returns>
        public abstract Weapon WeaponEquiped();

        /// <summary>
        /// Method to calculate total DPS
        /// </summary>
        /// <returns>
        /// total DPS
        /// </returns>
        public abstract int TotalHeroDPS();


        /// <summary>
        /// Method which levels up the hero. it takes in a parameter and updates currentlevel
        /// </summary>
        /// <param name="level"></param>
        public void LevelUp(int level)
        {
            if(level < 1)
            {
                throw new ArgumentException();
            }
            else
            {
                Level = Level + level;
                NewLevel = Level;
            }
        }

        /// <summary>
        /// Calulates total attributes. Adds primary attributes and amror attributes
        /// </summary>
        public void TotalHeroAttributes()
        {
            if (EquippedArmor == "Armor Equipped")
            {
                PrimaryAttribute = PrimaryAttribute + ArmorEquiped().ArmorPrimaryAttributes;
                TotalStrength = (int)PrimaryAttribute.Strength;
                TotalDexterity = (int)PrimaryAttribute.Dexterity;
                TotalVitality = (int)PrimaryAttribute.Vitality;
                TotalIntelligence = (int)PrimaryAttribute.Intelligence;
            }
            else
            {
                TotalStrength = (int)PrimaryAttribute.Strength;
                TotalDexterity = (int)PrimaryAttribute.Dexterity;
                TotalVitality = (int)PrimaryAttribute.Vitality;
                TotalIntelligence = (int)PrimaryAttribute.Intelligence;
            }
        }

        /// <summary>
        /// Sets total secondary attributes
        /// </summary>
        public void TotalSecondaryAttributes()
        {
            SecondaryAttribute = new SecondaryAttribute()
            {
                Health = TotalVitality * 10,
                ArmorRating = TotalStrength + TotalDexterity,
                ElementalResistance = TotalIntelligence
            };
        }
        
        /// <summary>
        /// Strtingbuilder to display stats
        /// </summary>
        public void DisplayStats()
        {
            myString.Append("Name: " + Name + "\n");
            myString.Append("Level: " + Level + "\n");
            myString.Append("Strength: " + TotalStrength + "\n");
            myString.Append("Dexterity: " + TotalDexterity + "\n");
            myString.Append("Intelligence: " + TotalIntelligence + "\n");
            myString.Append("Health: " + SecondaryAttribute.Health + "\n");
            myString.Append("Armor: " + SecondaryAttribute.ArmorRating + "\n");
            myString.Append("Elemental Resistance: " + SecondaryAttribute.ElementalResistance + "\n");
            myString.Append("Vitality: " + TotalVitality + "\n");
            myString.Append("Total DPS: " + totalDPS);

            Console.WriteLine(myString);
        }
    }
}
