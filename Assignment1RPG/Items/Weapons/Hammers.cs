﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1RPG
{
    public class Hammers :Weapon
    {
        public Hammers()
        {
            Name = EnumItemNames.Hammer;
            BaseDamage = 11;
            AttackSpeed = 2;
            ReqiuredLevel = 1;
            DPS = BaseDamage * AttackSpeed;
        }
    }
}
