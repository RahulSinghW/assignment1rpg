﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1RPG
{
    public abstract class Weapon : Item
    {
        public int BaseDamage { get; set; }
        public int AttackSpeed { get; set; }
        public int DPS { get; set; }

    }
}
