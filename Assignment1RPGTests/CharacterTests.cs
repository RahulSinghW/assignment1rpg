using Assignment1RPG;
using System;
using Xunit;

namespace Assignment1RPGTests
{
    public class CharacterTests
    {
        [Fact]
        public void Check_CheckCharacterLevel_ShouldReturnLevel()
        {
            //Arange
            Mage mage = new Mage();
            

            int Level = 1;
            int expected = Level;

            //Act
            int actual = mage.Level;

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void LevelUp_LevelUpCharacter_ShouldReturnLevel()
        {
            //Arange
            Mage mage = new Mage();
            mage.LevelUp(1);
            
            int Level = 2;
            int expected = Level;

            //Act
            int actual = mage.Level;

            //Assert
            Assert.Equal(expected, actual);

        }

        /*
        [Theory]
        [InlineData (0, -1)]
        public void LevelUp_LevelUpCharacter_ShouldReturnExeception(int a, int b)
        {
            //Arange
            Mage mage = new Mage();
            
            int Level = 2;
            int expected = Level;

            //Act
            int actual = mage.Level;

            //Act & Assert
            Assert.Throws<ArgumentException>(() => mage.LevelUp(a));
        }
        */
        [Fact]
        public void Check_CheckDefaultAttributesMage_ShouldReturnDefaultAttributesMage()
        {
            //Arrange
            Mage mage = new Mage();
            mage.CreateHero();

            int strength = 1;
            int dexterity = 1;
            int vitality = 5;
            int inteligence = 8;


            int[] expected = { strength, dexterity, vitality, inteligence };

            //Act
            int[] actual = {mage.BasePrimaryAttribute.Strength, mage.BasePrimaryAttribute.Dexterity,
                mage.BasePrimaryAttribute.Vitality , mage.BasePrimaryAttribute.Intelligence};

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void Check_CheckDefaultAttributesRanger_ShouldReturnDefaultAttributesRanger()
        {
            //Arrange
            Ranger ranger = new Ranger();
            ranger.CreateHero();

            int strength = 1;
            int dexterity = 7;
            int vitality = 8;
            int inteligence = 1;


            int[] expected = { strength, dexterity, vitality, inteligence };

            //Act
            int[] actual = {ranger.BasePrimaryAttribute.Strength, ranger.BasePrimaryAttribute.Dexterity,
                ranger.BasePrimaryAttribute.Vitality , ranger.BasePrimaryAttribute.Intelligence};

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void Check_CheckDefaultAttributesRogue_ShouldReturnDefaultAttributesRogue()
        {
            //Arrange
            Rogue rogue = new Rogue();
            rogue.CreateHero();

            int strength = 2;
            int dexterity = 6;
            int vitality = 8;
            int inteligence = 1;


            int[] expected = { strength, dexterity, vitality, inteligence };

            //Act
            int[] actual = {rogue.BasePrimaryAttribute.Strength, rogue.BasePrimaryAttribute.Dexterity,
                rogue.BasePrimaryAttribute.Vitality , rogue.BasePrimaryAttribute.Intelligence};

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void Check_CheckDefaultAttributesWarrior_ShouldReturnDefaultAttributesWarrior()
        {
            //Arrange
            Warrior warrior = new Warrior();
            warrior.CreateHero();

            int strength = 5;
            int dexterity = 2;
            int vitality = 10;
            int inteligence = 1;


            int[] expected = { strength, dexterity, vitality, inteligence };

            //Act
            int[] actual = {warrior.BasePrimaryAttribute.Strength, warrior.BasePrimaryAttribute.Dexterity,
                warrior.BasePrimaryAttribute.Vitality , warrior.BasePrimaryAttribute.Intelligence};

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void Check_CheckLevelUpAttributesMage_ShouldReturnLevelUpAttributesMage()
        {
            //Arrange
            Mage mage = new Mage();
            mage.LevelUp(1);
            mage.CreateHero();
            

            int strength = 2;
            int dexterity = 2;
            int vitality = 8;
            int inteligence = 13;


            int[] expected = { strength, dexterity, vitality, inteligence };

            //Act
            int[] actual = {mage.PrimaryAttribute.Strength, mage.PrimaryAttribute.Dexterity,
                mage.PrimaryAttribute.Vitality , mage.PrimaryAttribute.Intelligence};

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void Check_CheckLevelUpAttributesRanger_ShouldReturnLevelUpAttributesRanger()
        {
            //Arrange
            Ranger ranger = new Ranger();
            ranger.LevelUp(1);
            ranger.CreateHero();

            int strength = 2;
            int dexterity = 12;
            int vitality = 10;
            int inteligence = 2;


            int[] expected = { strength, dexterity, vitality, inteligence };

            //Act
            int[] actual = {ranger.PrimaryAttribute.Strength, ranger.PrimaryAttribute.Dexterity,
                ranger.PrimaryAttribute.Vitality , ranger.PrimaryAttribute.Intelligence};

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void Check_CheckLevelUpAttributesRogue_ShouldReturnLevelUpAttributesRogue()
        {
            //Arrange
            Rogue rogue = new Rogue();
            rogue.LevelUp(1);
            rogue.CreateHero();

            int strength = 3;
            int dexterity = 10;
            int vitality = 11;
            int inteligence = 2;


            int[] expected = { strength, dexterity, vitality, inteligence };

            //Act
            int[] actual = {rogue.PrimaryAttribute.Strength, rogue.PrimaryAttribute.Dexterity,
                rogue.PrimaryAttribute.Vitality , rogue.PrimaryAttribute.Intelligence};

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void Check_CheckLevelUpAttributesWarrior_ShouldReturnLevelUpAttributesWarrior()
        {
            //Arrange
            Warrior warrior = new Warrior();
            warrior.LevelUp(1);
            warrior.CreateHero();

            int strength = 8;
            int dexterity = 4;
            int vitality = 15;
            int inteligence = 2;


            int[] expected = { strength, dexterity, vitality, inteligence };

            //Act
            int[] actual = {warrior.PrimaryAttribute.Strength, warrior.PrimaryAttribute.Dexterity,
                warrior.PrimaryAttribute.Vitality , warrior.PrimaryAttribute.Intelligence};

            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void Check_CheckLevelUpSecondaryStatsWarrior_ShouldReturnLevelUpSecondaryStatsWarrior()
        {
            //Arrange
            Warrior warrior = new Warrior();
            warrior.LevelUp(1);
            warrior.CreateHero();
            warrior.TotalHeroAttributes();
            warrior.TotalSecondaryAttributes();

            int health = 150;
            int armorRating = 12;
            int elementalResistance = 2;


            int[] expected = { health, armorRating, elementalResistance };

            //Act
            int[] actual = {warrior.SecondaryAttribute.Health, warrior.SecondaryAttribute.ArmorRating,
                warrior.SecondaryAttribute.ElementalResistance};

            //Assert
            Assert.Equal(expected, actual);

        }
    }
}
