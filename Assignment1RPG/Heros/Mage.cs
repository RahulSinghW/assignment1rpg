﻿namespace Assignment1RPG
{
    public class Mage : Hero
    {
        Armor Armor;
        Weapon Weapon;
        public Mage()
        {
        }

        public override void CreateHero()
        {
            Name = EHeroNames.Mage;

            if (NewLevel > Level)
            {
                Level = NewLevel;
            }

            BasePrimaryAttribute = new PrimaryAttribute()
            {
                Strength = 1,
                Dexterity = 1,
                Vitality = 5,
                Intelligence = 8
            };

            HeroAttributeBoost = ((1) / 100) * (BasePrimaryAttribute.Intelligence + ((Level - 1) * 5));

            PrimaryAttribute = new PrimaryAttribute()
            {
                Strength = BasePrimaryAttribute.Strength + ((Level - 1) * 1),
                Dexterity = BasePrimaryAttribute.Dexterity + ((Level - 1) * 1),
                Vitality = BasePrimaryAttribute.Vitality + ((Level - 1) * 3),
                Intelligence = BasePrimaryAttribute.Intelligence + ((Level - 1) * 5) + HeroAttributeBoost
            };
        }

        

        public override string EquipWeapon(Weapon weapon)
        {

            if (weapon is not (Staff or Wands))
            {
                throw new InvalidWeaponException("Character cannot equip: " + weapon.Name);
            }
            else if (Level < weapon.ReqiuredLevel)
            {
                throw new InvalidWeaponException("You need to be level " + weapon.ReqiuredLevel + " to equip this weapon: " + weapon.Name);
            }
            else
            {
                EquippedWeapon = "Weapon Equipped";
                Weapon = weapon;
                return new string("New weapon equipped!");
            }

        }

        public override string EquipArmor(Armor armor)
        {
            if (armor is not Cloth)
            {
                throw new InvalidArmorException("Character cannot equip: " + armor.Name);
            }
            else if (Level < armor.ReqiuredLevel)
            {
                throw new InvalidArmorException("You need to be level " + armor.ReqiuredLevel + " to equip this armor: " + armor.Name);
            }
            else
            {
                EquippedArmor = "Armor Equipped";
                Armor = armor;
                return new string("New armour equipped!");
            }
        }

        public override Armor ArmorEquiped()
        {
            return Armor;

        }

        public override Weapon WeaponEquiped()
        {
            return Weapon;
        }


        public override int TotalHeroDPS()
        {
            if (EquippedWeapon == "Weapon Equipped")
            {
                totalDPS = WeaponEquiped().DPS * (1 + (int)TotalIntelligence / 100);
                return totalDPS;
            }
            else
            {
                totalDPS = 1;
                return totalDPS;
            }
        }

    }
}
