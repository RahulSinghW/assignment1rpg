﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1RPG
{
    public class Swords : Weapon
    {
        public Swords()
        {
            Name = EnumItemNames.Sword;
            BaseDamage = 7;
            AttackSpeed = 5;
            ReqiuredLevel = 6;
            DPS = BaseDamage * AttackSpeed;
        }
    }
}
