﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1RPG
{
    public class Leather : Armor
    {

        public Leather()
        {
            Name = EnumItemNames.Leather;
            ReqiuredLevel = 5;
            PrimaryAttribute armorPrimaryAttributes = new PrimaryAttribute()
            {
                Strength = 3,
                Dexterity = 2,
                Vitality = 4,
                Intelligence = 2
            };
        }

    }
}
